<?php
class healthy extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('healthy_model');
                $this->load->helper('url_helper');
        }

        public function index()
        {
                $data['healthy'] = $this->healthy_model->get_healthy();
        }

        public function view($slug = NULL)
        {
                $data['healthy_item'] = $this->healthy_model->get_healthy($slug);
        }
	
		public function create()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Healthy List';

		$this->form_validation->set_rules('title', 'Title', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('healthy/create');
			$this->load->view('templates/footer');

		}
		else
		{
			$this->healthy_model->set_healthy();
			$this->load->view('healthy/success');
		}
	}
}

