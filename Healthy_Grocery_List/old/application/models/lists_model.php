<?php
class lists_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
		
		public function get_healthy_food($slug = FALSE)
{
        if ($slug === FALSE)
        {
                $query = $this->db->get('healthy_food');
                return $query->result_array();
        }

        $query = $this->db->get_where('healthy_food', array('slug' => $slug));
        return $query->row_array();
}
		public function set_healthy()
{
		$this->load->helper('url');

		$slug = url_title($this->input->post('title'), 'dash', TRUE);

		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'text' => $this->input->post('text')
		);

		return $this->db->insert('healthy_food', $data);
	}

}
?>