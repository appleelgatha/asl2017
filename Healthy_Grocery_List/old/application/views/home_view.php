<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Healthy Grocery List</title>
	<!--link the master css file-->
	<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/master.css"> 
	
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div  class = "welcome">
				<div class="containerW">
					<div class="contentW">
						<div class="title m-b-md">
							Welcome to
						</div>
						<div class="title m-b-md">
							Healthy
						</div>
						<div class="title m-b-md">
							Grocery List
						</div>
					</div>
				</div>
			</div>
			<ul class="nav navbar-nav">
				<?php if ($this->session->userdata('login')){ ?>
				<li><p class="navbar-text-hi">Hello <?php echo $this->session->userdata('uname'); ?></p></li>
				<li class="logs"><a href="<?php echo base_url(); ?>index.php/home/logout/index">Log Out</a></li>
				<?php } else { ?>
				<li><a href="<?php echo base_url(); ?>index.php/login/index">Login</a></li>
				<li class="logs-line">&nbsp;|&nbsp;</li>
				<li><a href="<?php echo base_url(); ?>index.php/signup/index">Signup</a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery-1.10.2.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
</body>
</html>
