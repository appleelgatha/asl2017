<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Healthy Grocery List</title>
	<!--link the master css file-->
	<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/master.css"> 
	
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div  class = "welcome">
				<div class="containerW">
					<div class="contentW">
						<div class="title m-b-md">
							You were
						</div>
						<div class="title m-b-md">
							successful in 
						</div>
						<div class="title m-b-md">
							creating your list
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery-1.10.2.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
</body>
</html>
