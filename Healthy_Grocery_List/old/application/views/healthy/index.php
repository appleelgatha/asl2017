<h2><?php echo $title; ?></h2>

<?php foreach ($healthy as $healthy_item): ?>

        <h3><?php echo $healthy_item['title']; ?></h3>
        <div class="main">
                <?php echo $healthy_item['text']; ?>
        </div>
        <p><a href="<?php echo site_url('healthy/'.$healthy_item['slug']); ?>">View article</a></p>

<?php endforeach; ?>
