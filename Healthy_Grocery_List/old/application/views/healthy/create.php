<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Healthy Grocery List</title>
	<!--link the master css file-->
	<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/master.css"> 
	
</head>
<body>
<h2 style="text-align: center;">Create Your Grocery List</h2>

<?php echo validation_errors(); ?>

<?php echo form_open('healthy/create'); ?>

    <label for="title">Title</label>
    <input type="input" name="title" /><br />

    <br />
    <input type="submit" name="insert" value="Insert" />
	<input type="submit" name="update" value="Update" />
	<input type="submit" name="delete" value="Delete" />

    <input type="submit" name="submit" value="Create healthy item" />

</form>
	</body>
</html>