<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// General
$lang['system_system_name'] = "Grocery Shopping List";

// Lists - view.php
$lang['lists_add_list'] = "Add";
$lang['lists_add_list_desc'] = "What do you need to purchase?";
$lang['lists_list_desc'] = "List Description";
$lang['lists_set_done'] = "Mark as done";
$lang['lists_set_todo'] = "Mark as todo";
$lang['list_due_d'] = "Due Day";
$lang['list_due_m'] = "Due Month";
$lang['list_due_y'] = "Due Year";
$lang['status_change_success'] = "List updated";
$lang['status_change_fail'] = "List not updated";