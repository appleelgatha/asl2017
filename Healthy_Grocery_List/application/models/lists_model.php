<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lists_model extends CI_Model {
  function __construct() {
    parent::__construct();
  }
	function get_lists() {
  $query = "SELECT * FROM `lists` ";

  $result = $this->db->query($query);
  if ($result) {
    return $result;
  } else {
    return false;
  }
}
function change_list_status($list_id, $save_data) {
  $this->db->where('list_id', $list_id);
  if ($this->db->update('lists', $save_data)) {
    return true;
  } else {
    return false;
  }
}
	function save_list($save_data) {
  if ($this->db->insert('lists', $save_data)) {
    return true;
  } else {
    return false;
  }
}
	function get_list($id) {
  $this->db->where('list_id', $id);
  $result = $this->db->get('lists');
  if ($result) {
    return $result;
  } else {
    return false;
  }
}
	 function delete($id) {
    $this->db->where('list_id', $id);
    $result = $this->db->delete('lists');
    if ($result) {
      return true;
    } else {
      return false;
    }
  }
}