<div class="page-header">
    <?php echo form_open('grocery_lists/index') ; ?>
      <div class="row">
        <div class="col-lg-12">
          <?php echo validation_errors() ; ?>
          <div class="input-group">
            <input type="text" class="form-control" name="grocery_desc" placeholder="<?php echo $this->lang->line('grocery_lists_add_grocery_desc'); ?>">
            <span class="input-group-btn">
              <button class="btn btn-default" type="submit"><?php echo $this->lang->line('grocery_lists_add_grocery'); ?></button>
            </span>
          </div><!-- /input-group -->
        </div><!-- /.col-lg-6 -->
      </div><!-- /.row -->
        </div>
      </div> 
    <?php echo form_close() ; ?>
</div>
<table class="table table-hover">
  <?php foreach ($query->result() as $row) : ?> 
  <?php if ($row->task_due_date == null) {echo ' <tr>';} ?>
    <td width="80%"><?php if ($row->task_status == 'done') {echo '<strike>'.$row->task_desc.'</strike>' ;} else {echo $row->task_desc;} ?>

    </td>
    <td width="10%">
      <?php if ($row->task_status == 'todo') {echo anchor ('grocery/status/done/'.$row->task_id, 'It\'s Done');} ?>
      <?php if ($row->task_status == 'done') {echo anchor ('grocery/status/todo/'.$row->task_id, 'Still Todo');} ?>
    </td>
    <td width="10%"><?php echo anchor ('grocery/delete/'.$row->task_id, $this->lang->line('common_form_elements_action_delete')) ; ?>
    </td>
  </tr>
<?php endforeach ; ?>
</table>