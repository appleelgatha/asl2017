<div class="page-header" style="margin-top: 200px; border-bottom: 1px solid transparent;">
    <?php echo form_open('lists/index') ; ?>
      <div class="row">
        <div class="col-lg-6">
          <?php echo validation_errors() ; ?>
          <div class="input-group">
            <input type="text" class="form-control" name="list_desc" placeholder="<?php echo $this->lang->line('lists_add_list_desc'); ?>">
            <span class="input-group-btn">
              <button class="btn btn-default" type="submit" style="height: 35px;"><?php echo $this->lang->line('lists_add_list'); ?></button>
            </span>
          </div><!-- /input-group -->
          <div class="row">
		<div class="col-md-4 col-md-offset-4 text-center">	
		<a href="<?php echo base_url(); ?>index.php/home/">Return to Home</a>
		</div>
	</div>
        </div><!-- /.col-lg-6 -->
      </div><!-- /.row -->
      
    <?php echo form_close() ; ?>

<div class="col-lg-6">
<table class="table table-hover">
  <?php foreach ($query->result() as $row) : ?> 
  <?php if (date("Y-m-d",mktime(0, 0, 0, date('m'), date('d'), date('y'))) > $row->list_due_date) {echo ' <tr class="list-group-item-danger">';} ?>
  <?php if ($row->list_due_date == null) {echo ' <tr>';} ?>
    <td width="80%"><?php if ($row->list_status == 'done') {echo '<strike>'.$row->list_desc.'</strike>' ;} else {echo $row->list_desc;} ?>

    </td>
    <td width="10%">
      <?php if ($row->list_status == 'todo') {echo anchor ('lists/status/done/'.$row->list_id, 'It\'s Done');} ?>
      <?php if ($row->list_status == 'done') {echo anchor ('lists/status/todo/'.$row->list_id, 'Still Todo');} ?>
    </td>
    <td width="10%"><?php echo anchor ('lists/delete/'.$row->list_id, $this->lang->line('common_form_elements_action_delete')) ; ?>
    </td>
  </tr>
<?php endforeach ; ?>
</table>
</div>
</div>