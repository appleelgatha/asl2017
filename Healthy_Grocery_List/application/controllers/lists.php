<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends MY_Controller {
  function __construct() {
  parent::__construct();
    $this->load->helper('string');
    $this->load->helper('text');
    $this->load->model('Lists_model');
    $this->load->library('form_validation');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
  }
	
	public function index() {
  $this->form_validation->set_rules('list_desc', $this->lang->line('lists_list_desc'), 'required|min_length[1]|max_length[255]');
  $this->form_validation->set_rules('list_due_d', $this->lang->line('list_due_d'), 'min_length[1]|max_length[2]');
  $this->form_validation->set_rules('list_due_m', $this->lang->line('list_due_m'), 'min_length[1]|max_length[2]');
  $this->form_validation->set_rules('list_due_y', $this->lang->line('list_due_y'), 'min_length[4]|max_length[4]');
if ($this->form_validation->run() == FALSE) {
  $page_data['job_title'] = array('name' => 'job_title', 'class' => 'form-control', 'id' => 'job_title', 'value' => set_value('job_title', ''), 'maxlength' => '100', 'size' => '35');
  $page_data['list_desc'] = array('name' => 'list_desc', 'class' => 'form-control', 'id' => 'list_desc', 'value' => set_value('list_desc', ''), 'maxlength' => '255', 'size' => '35');
  $page_data['list_due_d'] = array('name' => 'list_due_d', 'class' => 'form-control', 'id' => 'list_due_d', 'value' => set_value('list_due_d', ''), 'maxlength' => '100', 'size' => '35');
  $page_data['list_due_m'] = array('name' => 'list_due_m', 'class' => 'form-control', 'id' => 'list_due_m', 'value' => set_value('list_due_m', ''), 'maxlength' => '100', 'size' => '35');
  $page_data['list_due_y'] = array('name' => 'list_due_y', 'class' => 'form-control', 'id' => 'list_due_y', 'value' => set_value('list_due_y', ''), 'maxlength' => '100', 'size' => '35');
	
	
	  $page_data['query'] = $this->Lists_model->get_lists();

  $this->load->view('common/header');
  $this->load->view('nav/top_nav');
  $this->load->view('lists/view', $page_data);
  $this->load->view('common/footer');
} else {
	
	if ($this->input->post('list_due_y') && $this->input->post('list_due_m') && $this->input->post('list_due_d')) {
  $list_due_date = $this->input->post('list_due_y') .'-'. $this->input->post('list_due_m') .'-'. $this->input->post('list_due_d');
} else {
  $list_due_date = null;
}
	
	$save_data = array(
  'list_desc' => $this->input->post('list_desc'),
  'list_due_date' => $list_due_date,
  'list_status' => 'todo'
);
	  if ($this->Lists_model->save_list($save_data)) {
      $this->session->set_flashdata('flash_message', $this->lang->line('create_success_okay'));
    } else {
      $this->session->set_flashdata('flash_message', $this->lang->line('create_success_fail'));
    }
    redirect ('lists'); 
  }
}
	public function status() {
  $page_data['list_status'] = $this->uri->segment(3);
  $list_id = $this->uri->segment(4);
		
		if ($this->Lists_model->change_list_status($list_id, $page_data)) {
    $this->session->set_flashdata('flash_message', $this->lang->line('status_change_success'));
  } else {
    $this->session->set_flashdata('flash_message', $this->lang->line('status_change_fail'));
  }
  redirect ('lists');
}
	public function delete() {
  $this->form_validation->set_rules('id', $this->lang->line('list_id'), 'required|min_length[1]|max_length[11]|integer|is_natural');
		if ($this->input->post()) {
  $id = $this->input->post('id');
} else {
  $id = $this->uri->segment(3);
}

$data['page_heading'] = 'Confirm delete?';
if ($this->form_validation->run() == FALSE) {
	
	  $data['query'] = $this->Lists_model->get_list($id);
  $this->load->view('templates/header', $data);
  $this->load->view('nav/top_nav', $data);
  $this->load->view('lists/delete', $data);
  $this->load->view('templates/footer', $data);
} else {
	 if ($this->Lists_model->delete($id)) {
        redirect('lists');
      }
    }
  }
}