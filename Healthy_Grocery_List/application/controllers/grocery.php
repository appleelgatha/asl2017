<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class grocery extends CI_Controller {
     
    function grocery()
    {
        parent::__construct();
         
        if(!$this->session->userdata('logged'))
            redirect('login');
    }
     
    public function index()
    {
        redirect('dashboard');
    }
     
    public function lists($grocery_id)
    {
        // Load lists - TODO: next
 
        // Load grocery info
        $this->load->model('grocery_model');
        $grocery = $this->grocery_model->get($grocery_id);
         
        $data['page_title'] = "grocery: ".$grocery['name'];
        $data['grocery']    = $grocery_id;
         
        $data['current_user'] = $this->session->userdata('user');
         
        $db_users = $this->grocery_model->get_related_users($grocery_id);
        $users = array();
        foreach ($db_users as $user) {
            $users[$user['id']] = $user;
        }
        $data['users'] = $users;
         
        // Load text helper to be used in the view
        $this->load->helper('text');
         
        // Load View
        $this->template->show('task_board', $data);
    }
 
    public function add()
    {
        $this->load->model('grocery_model');
         
        $data['page_title']  = "New grocery";
        $data['user']        = '';
        $data['name']        = '';
        $data['description'] = '';
         
        $users = $this->grocery_model->get_related_users();
        foreach ($users as $key => $value) {
            if($value['id'] == $this->session->userdata('user'))
                $users[$key]['grocery'] = 1;
            else
                $users[$key]['grocery'] = 0;
        }
        $data['users'] = $users;
         
        $this->template->show('grocery_add', $data);
    }
 
    public function edit($id)
    {
        $this->load->model('grocery_model');
         
        $data = $this->grocery_model->get($id);
         
        $data['page_title']  = "Edit grocery #".$id;
        $data['grocery']  = $id;
        $data['users'] = $this->grocery_model->get_related_users($id);
         
        $this->template->show('grocery_add', $data);
    }
     
    public function save()
    {
        $this->load->model('grocery_model');
         
        $sql_data = array(
            'user'        => $this->session->userdata('user'),
            'name'        => $this->input->post('name'),
            'description' => $this->input->post('description')
        );
         
        $grocery_id = $this->input->post('id');
         
        if ($grocery_id)
            $this->grocery_model->update($grocery_id,$sql_data);
        else
            $grocery_id = $this->grocery_model->create($sql_data);
             
        // Related users
        $this->grocery_model->delete_related($grocery_id);
         
        $users = $this->input->post('users');
        foreach ($users as $user) {
            $sql_data = array(
                'user' => $user,
                'grocery' => $grocery_id
            );
            $this->grocery_model->create_related($sql_data);
        }
 
        if ($grocery_id)
            redirect('home/index'.$grocery_id);
        else
            redirect('home/index');
    }
}